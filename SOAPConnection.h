//
//  SOAPConnection.h
//
//  Created by Fabio Daneze on 12/02/15.
//  Copyright (c) 2015 Beelieve. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    kErrorNoError = 0,
    kErrorNoConnection = 1,
    kErrorNoResponse = 2,
    kErrorNoData = 3,
    kErrorUnknown = 4,
} ConnectionErrorType;

@interface SOAPConnection : NSObject

- (id)searchNode:(NSString *)node fromBody:(NSDictionary *)body;

- (void)requestSOAPAction:(NSString *)action
               parameters:(NSDictionary *)parameters
                 response:(void (^)(NSDictionary *responseBody,
                                    ConnectionErrorType errorValue)) response;

@end
