//
//  SOAPConnection.m
//
//  Created by Fabio Daneze on 12/02/15.
//  Copyright (c) 2015 Beelieve. All rights reserved.
//

#import "SOAPConnection.h"
#import <XMLDictionary/XMLDictionary.h>

#define kTimeoutInterval 10

#define baseURL @"http://domain.com/"
#define serviceURL [baseURL stringByAppendingString:@"Service.apw"]

#define prefix @"xmlPrefix"


@implementation SOAPConnection

- (NSString *)startTagXML:(NSString *)tag
{
    return [NSString stringWithFormat:@"\n<%@:%@>", prefix, tag];
}

- (NSString *)endTagXML:(NSString *)tag
{
    return [NSString stringWithFormat:@"\n</%@:%@>", prefix, tag];
}

- (NSString *)tagXML:(NSString *)tag value:(NSString *)value
{
    return [NSString stringWithFormat:@"\n<%@:%@>%@</%@:%@>", prefix, tag, value, prefix, tag];
}

- (NSString *)SOAPMessageForObject:(id)object key:(NSString *)key
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    if ([object isKindOfClass:[NSDictionary class]])
    {
        [message appendString:[self startTagXML:key]];
        
        [message appendString:[self SOAPMessageForDictionary:object]];
        
        [message appendString:[self endTagXML:key]];
        
    }
    else if ([object isKindOfClass:[NSArray class]])
    {
        [message appendString:[self SOAPMessageForArray:object key:key]];
    }
    else
    {
        [message appendString:[self tagXML:key value:object]];
    }
    
    return message;
}

- (NSString *)SOAPMessageForDictionary:(NSDictionary *)dictionary
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    for (NSString *key in dictionary.allKeys)
    {
        
        if ([dictionary[key] isKindOfClass:[NSDictionary class]])
        {
            [message appendString:[self startTagXML:key]];
            
            [message appendString:[self SOAPMessageForDictionary:dictionary[key]]];
            
            [message appendString:[self endTagXML:key]];
        }
        else if ([dictionary[key] isKindOfClass:[NSArray class]])
        {
            [message appendString:[self SOAPMessageForArray:dictionary[key] key:key]];
        }
        else
        {
            [message appendString:[self tagXML:key value:dictionary[key]]];
        }
        
    }
    
    return message;
}

- (NSString *)SOAPMessageForArray:(NSArray *)array key:(NSString *)key
{
    NSMutableString *message = [[NSMutableString alloc] init];
    
    for (id object in array)
    {
        
        if ([object isKindOfClass:[NSDictionary class]])
        {
            [message appendString:[self startTagXML:key]];
            
            [message appendString:[self SOAPMessageForDictionary:object]];
            
            [message appendString:[self endTagXML:key]];
        }
        else if ([object isKindOfClass:[NSArray class]])
        {
            [message appendString:[self SOAPMessageForArray:object key:key]];
        }
        else
        {
            [message appendString:[self tagXML:key value:object]];
        }
        
    }
    
    return message;
}

- (NSString *)SOAPBodyForParameters:(NSDictionary *)parameters action:(NSString *)action
{
    NSMutableString *message = [[NSMutableString alloc] initWithFormat:
                                @"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:%@=\"%@\">", prefix, baseURL];
    
    if (parameters)
    {
        [message appendString:@"\n<soapenv:Body>"];
        
        [message appendString:[self SOAPMessageForObject:parameters key:action]];
        
        [message appendString:@"\n</soapenv:Body>"];
    }
    else
    {
        [message appendString:@"<soapenv:Body/>\n"];
    }
    
    [message appendString:@"\n</soapenv:Envelope>"];
    
    return message;
}

- (NSDictionary *)SOAPHeaderForAction:(NSString *)action andBodyLength:(NSUInteger)bodyLength
{
    NSMutableDictionary *header = [[NSMutableDictionary alloc] init];
    header[@"Content-Type"] = @"text/xml";
    
    if (action) {
        header[@"SOAPAction"] = [baseURL stringByAppendingString:action];
    }
    
    header[@"Content-Length"] = @(bodyLength).stringValue;
    
    return header;
}

- (id)SOAPBodyFromResponse:(id)response
{
    if ([response isKindOfClass:[NSDictionary class]])
    {
        return [response objectForKey:@"soap:Body"];
    }
    
    return nil;
}

- (id)searchNode:(NSString *)node fromBody:(NSDictionary *)body
{
    NSMutableArray *arrayParent = [NSMutableArray array];
    NSMutableArray *arrayCount = [NSMutableArray array];
    NSMutableArray *arrayLimit = [NSMutableArray array];
    
    id searchAux = body;
    NSInteger count = 0;
    NSInteger limit = body.allKeys.count;
    
    id key, obj;
    
    while (count < limit) {
        
        if ([searchAux isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *dictAux = (NSDictionary *)searchAux;
            key = dictAux.allKeys[count];
            obj = dictAux[key];
        }
        else if ([searchAux isKindOfClass:[NSArray class]])
        {
            NSArray *arrayAux = (NSArray *)searchAux;
            obj = arrayAux[count];
        }
        
        if (key && [key isEqualToString:node])
        {
            return obj;
        }
        else if ([obj isKindOfClass:[NSDictionary class]])
        {
            [arrayParent addObject:searchAux];
            [arrayCount addObject:[NSNumber numberWithInteger:count]];
            [arrayLimit addObject:[NSNumber numberWithInteger:limit]];
            
            searchAux = obj;
            count = 0;
            NSDictionary *dictAux = (NSDictionary *)obj;
            limit = dictAux.allKeys.count;
        }
        else if ([obj isKindOfClass:[NSArray class]])
        {
            [arrayParent addObject:searchAux];
            [arrayCount addObject:[NSNumber numberWithInteger:count]];
            [arrayLimit addObject:[NSNumber numberWithInteger:limit]];
            
            searchAux = obj;
            count = 0;
            NSArray *arrayAux = (NSArray *)obj;
            limit = arrayAux.count;
        }
        else
        {
            count++;
            
            if (count>=limit && arrayParent.count>0)
            {
                searchAux = [arrayParent lastObject];
                count = [[arrayCount lastObject] integerValue];
                limit = [[arrayLimit lastObject] integerValue];
                
                [arrayParent removeLastObject];
                [arrayCount removeLastObject];
                [arrayLimit removeLastObject];
                
                count++;
            }
        }
        
        key = nil;
        obj = nil;
    }
    
    return nil;
}

- (void)requestSOAPAction:(NSString *)action
               parameters:(NSDictionary *)parameters
                 response:(void (^)(NSDictionary *responseBody,
                                     ConnectionErrorType errorValue)) response
{   
    NSString *body = [self SOAPBodyForParameters:parameters action:action];
    
    NSData *bodyData = [body dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *header = [self SOAPHeaderForAction:action andBodyLength:bodyData.length];
    
    [self postService:serviceURL
               header:header
                 body:bodyData
             response:^(NSData *responseData,
                        ConnectionErrorType errorValue) {
        
                 id responseObject = [NSDictionary dictionaryWithXMLData:responseData];
                 
                 id body = [self SOAPBodyFromResponse:responseObject];
        
                 response(body, errorValue);
                 
    }];
}

- (void)connectionService:(NSString *)service
                   method:(NSString *)method
                   header:(NSDictionary *)header
                     body:(NSData *)body
                 response:(void (^)(NSData *responseData,
                                    ConnectionErrorType errorValue)) response
{
    NSURL *target = [NSURL URLWithString:service];
        
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:target];
    [urlRequest setTimeoutInterval:kTimeoutInterval];
    
    [urlRequest setHTTPMethod:method];
    
    if (header)
    {
        for (NSString *key in header.allKeys) {
            [urlRequest addValue:header[key] forHTTPHeaderField:key];
        }
    }
    
    if (body) {
        [urlRequest setHTTPBody:body];
    }
    
    NSError *error = nil;
    NSData *data = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:nil error:&error];
    
    if (data)
    {
        response(data, kErrorNoError);
    }
    else
    {
        if ([error.localizedDescription isEqualToString:@"The request timed out."])
            response(nil, kErrorNoResponse);
        else
            if ([error.localizedDescription isEqualToString:@"The Internet connection appears to be offline."])
            response(nil, kErrorNoConnection);
        else
            response(nil, kErrorUnknown);
    }
}

- (void)postService:(NSString *)service
             header:(NSDictionary *)header
               body:(NSData *)body
           response:(void (^)(NSData *responseData,
                              ConnectionErrorType errorValue)) response
{
    [self connectionService:service method:@"POST" header:header body:body response:response];
}

@end
